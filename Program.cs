﻿using System;

namespace GeekBrains.CSharpTypes
{
    public partial class Program
    {
        public static void Main(string[] args)
        {
            CopyTypesSample();
            //CheckedMethod();
            //UncheckedMethod();
            //ImplicitSample();
            //ExplicitSample();
            //HashSample();
            Console.ReadLine();
        }
    }
}
