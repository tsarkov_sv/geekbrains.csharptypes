﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeekBrains.CSharpTypes
{
	public  class DigitExplicit
	{
		byte value;

		public byte Value { get => value; }

		public DigitExplicit(byte value)  
		{
			this.value = value;
		}

		public static explicit operator DigitExplicit(byte b)
		{
			DigitExplicit d = new DigitExplicit(b);

			Console.WriteLine("Conversion occurred.");
			return d;
		}

		public static explicit operator byte(DigitExplicit b)
		{
			return b.Value;
		}
	}

	public partial class Program
	{
		public static void ExplicitSample()
		{
			DigitExplicit dig = new DigitExplicit(7);

			byte num = (byte)dig;

			DigitExplicit dig2 = (DigitExplicit)num;
			Console.WriteLine("num = {0} dig2 = {1}", num, dig2.Value);
			Console.ReadLine();
		}
	}
}
