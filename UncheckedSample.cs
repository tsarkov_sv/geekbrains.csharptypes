﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeekBrains.CSharpTypes
{
    public partial class Program
    {
		static int UncheckedMethod()
		{
			int z = 0;

			try
			{
				z = maxIntValue + 10;
			}
			catch (System.OverflowException e)
			{
				Console.WriteLine("UNCHECKED and CAUGHT:  " + e.ToString());
			}

			return z;
		}
	}
}
