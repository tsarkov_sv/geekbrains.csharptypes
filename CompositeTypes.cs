using System;

namespace GeekBrains.CSharpTypes
{
    struct State
    {
        public int x;
        public int y;
        public Country country;
    }
    class Country
    {
        public int x;
        public int y;
    }

    public partial class Program
    {
        public static void CompositeSample()
        {
            State state1 = new State();
            state1.country = new Country();
            Country country1 = new Country();
        }
    }
}