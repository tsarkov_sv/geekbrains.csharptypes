﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeekBrains.CSharpTypes
{
    public partial class Program
    {
		static int maxIntValue = 2147483647;

		static int CheckedMethod()
		{
			int z = 0;
			try
			{
				checked
				{
					z = maxIntValue + 10;
				}
				//z = checked(maxIntValue + 10);
			}
			catch (System.OverflowException e)
			{
				Console.WriteLine("CHECKED and CAUGHT:  " + e.ToString());
			}

			return z;
		}
	}
}
