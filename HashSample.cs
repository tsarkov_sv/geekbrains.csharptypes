﻿using System;

namespace GeekBrains.CSharpTypes
{
    public class HashClass
    {
        public int Prop { get; set; }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    
    public partial class Program 
    {
        public static void HashSample()
        {
            var hash = new HashClass();
            hash.Prop = 12;
            Console.WriteLine(hash.GetHashCode());
            var hash2 = hash;
            Console.WriteLine(hash2.GetHashCode());
            Console.WriteLine(hash.Equals(hash2));
        }
    }
}