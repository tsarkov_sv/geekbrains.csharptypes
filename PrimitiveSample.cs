using System;

namespace GeekBrains.CSharpTypes
{
    public partial class Program
    {
        public static void PrimitiveSample()
        {
            Int32 fullNameInteger = 4;
            int shortNameInteger = 4;
            Int16 fullNameShort = 4;
            short shortNameShort = 4;
            String fullNameString = "4";
            string shortNameString = "4";
            var UnknownNameInteger = 4;
            var UnknownNameDouble = 4.0;
            Byte fullNameByte = 4;
            byte shortNameByte = 4;
        }
    }
}