﻿using System;

namespace GeekBrains.CSharpTypes
{
	public class DigitImplicit
	{
		private byte _value;

		public byte Value { get => _value; }

		public DigitImplicit(byte b)
		{
			this._value = b;
		}

		public static implicit operator byte(DigitImplicit d)
		{
			return d.Value;
		}

		public static implicit operator DigitImplicit(byte d)
		{
			return new DigitImplicit(d);
		}
	}

	public partial class Program
    {
		public static void ImplicitSample()
		{
			DigitImplicit dig = new DigitImplicit(7);

			byte num = dig;

			DigitImplicit dig2 = num;
			Console.WriteLine("num = {0} dig2 = {1}", num, dig2.Value);
			Console.ReadLine();
		}
    }
}
